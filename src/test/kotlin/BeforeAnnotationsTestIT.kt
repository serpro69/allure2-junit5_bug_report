import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class BeforeAnnotationsTestIT {

    @BeforeAll
    fun `this should be executed before all tests`() {}

    @BeforeEach
    fun `this should be executed before each test`() {}

    @AfterEach
    fun `this should be executed after each test`() {}

    @Test
    fun `test if before methods are present in report`() {
        println("Testing for @Before/@After annotated methods")
    }
}